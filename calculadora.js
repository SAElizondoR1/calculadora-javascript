function obtenerTexto() {
    return document.getElementById("texto").innerHTML
}

function establecerTexto(valor) {
    document.getElementById("texto").innerHTML = valor;
}

function borrarTexto() {
    establecerTexto("0");
}

function apilarAlTexto(valor) {
    var texto = obtenerTexto();
    if(texto == "0") {
        texto = "";
        establecerTexto(texto);
    }
    establecerTexto(texto + valor);
}

function resolver() {
    var texto = obtenerTexto();
    var respuesta = Function(`'use strict'; return (${texto})`)();
    establecerTexto(respuesta);
}
